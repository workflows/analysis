#!/usr/bin/python

import sys, getopt
import argparse
from ROOT import TH1F,TCanvas,TFile

parser = argparse.ArgumentParser()
parser.add_argument("-inputfile1",  type=str,   default="", help="")
parser.add_argument("-scalevalue1", type=float, default="", help="")
parser.add_argument("-inputfile2",  type=str,   default="", help="")
parser.add_argument("-scalevalue2", type=float, default="", help="")
parser.add_argument("-p",           type=str,   default="", help="")
parser.add_argument("-output",      type=str,   default="", help="")

args = parser.parse_args()

inputfile1  = args.inputfile1
scalevalue1 = float(args.scalevalue1)
inputfile2  = args.inputfile2
scalevalue2 = float(args.scalevalue2)
histname    = args.p     
output      = args.output 

# print the relevant data for running
print('Input file 1 is   : ', inputfile1)
print('Scale value 1 is  : ', scalevalue1)
print('Input file 2 is   : ', inputfile2)
print('Scale value 2 is  : ', scalevalue2)
print('Hist to plot is   : ', histname)
print('Output file is    : ', output)

# extract and draw the histogram  
fin1 = TFile(inputfile1)
h1 = fin1.Get(histname)
h1.Scale(scalevalue1)

fin2 = TFile(inputfile2)
h2 = fin2.Get(histname)
h2.Scale(scalevalue2)

h1.Add(h2)

fout = TFile(output,"RECREATE")
h1.Write(histname)
fout.Close()
        
